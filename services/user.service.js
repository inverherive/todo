﻿var config = require('config.json');
var _ = require('lodash');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var Q = require('q');
var users = [];

var service = {};

service.authenticate = authenticate;
service.getById = getById;
service.create = create;

module.exports = service;

function authenticate(username, password) {
    var deferred = Q.defer();
    var u = users.find(o => o.username == username);
    if (!u) {
        deferred.reject("User or password not valid");
    }
    else {
        if (u && bcrypt.compareSync(password, u.hash)) {
            // auth successful
            deferred.resolve(jwt.sign({ sub: u._id }, config.secret));
        } else {
            // auth failed
           deferred.reject("User or password not valid");
        }
    };
    return deferred.promise;
}

function getById(_id) {
    var deferred = Q.defer();
    var u = users.find(o=>o._id == _id);
    if (u) {
            deferred.resolve(_.omit(u, 'hash'));
    } else {
            // user not found
             deferred.resolve();
    }
    return deferred.promise;
}

function create(userParam) {
    var deferred = Q.defer();
    var u = users.find(o => o.username == userParam.username)
    if (u) {
        deferred.reject('Username "' + userParam.username + '" is already taken');
    } else {
        createUser();
    }

    function createUser() {
        var user = _.omit(userParam, 'password');

        user.hash = bcrypt.hashSync(userParam.password, 10);
        users.push(user);
        deferred.resolve();
    }
    return deferred.promise;
}
