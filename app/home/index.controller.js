﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('Home.IndexController', Controller);

    function Controller(UserService) {
        var tdl = this;
        tdl.modifyID = -1;
        tdl.count = 0;
        tdl.todos = [];
        tdl.user = null;
        tdl.formTodoText = "";
        tdl.addTodo = addTodo;
        tdl.modifyTodo = modifyTodo;
        tdl.clearCompleted = clearCompleted;
        initController();

        function initController() {
            // get current user
            UserService.GetCurrent().then(function (user) {
                tdl.user = user;
            });
        }
     
        function modifyTodo(id) {
            tdl.formTodoText = tdl.todos.find(o => o.id == id).text;
            tdl.modifyID = id;
        }
        
        function addTodo() {
            if (tdl.modifyID > -1) {
                tdl.todos = tdl.todos.map(function(item) {
                    if (item.id == tdl.modifyID) {
                        item.text = tdl.formTodoText;
                    }
                    return item;
                })
                tdl.modifyID = -1;
                tdl.formTodoText = '';
            }
            else {
                tdl.todos.push({id:tdl.count, text:tdl.formTodoText, done:false});
                tdl.formTodoText = '';
                tdl.count++;
            }
        }
        
        function clearCompleted() {
            
            tdl.todos = tdl.todos.filter(function(todo){
                    return !todo.done;
            });
        }

    }
})();