# README #

Example todo system to demonstrate secure login/registration.

## Caveats ##

* No data is saved to the backend between login sessions.
* No DB storage in this version

## Installation ##

Clone the repository to a local folder and issue:
```
npm install
npm start
```
Connect to system at:
```
http://localhost:3005
```